#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;
use Mojo::Collection 'c';
use Mojo::DOM;
use Mojo::File;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode encode url_unescape);
use Mojo::JSON qw(decode_json encode_json true false);
use Mojolicious;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use DateTime;
use DateTime::Format::RFC3339;
use DateTime::Locale;
use File::Copy::Recursive qw(dircopy fcopy);
use File::Spec qw(catfile);
use File::Path qw(rmtree);
use FindBin qw($Bin);
use Text::Slugify 'slugify';
use XML::Atom::SimpleFeed;
use Pandoc;
use Scalar::Util qw(reftype);
use POSIX;
use Math::Round qw(nearest);
use lib '.';

use vars qw($lc_loaded);
BEGIN {
    eval "use LastCustom";
    if ($@) {
        $lc_loaded = 0;
    } else {
        $lc_loaded = 1;
    }
}

# Get config
my $m      = Mojolicious->new();
my $ua     = Mojo::UserAgent->new();
my $config = $m->plugin(Config => {
    file    => File::Spec->catfile($Bin, 'last.conf'),
    default => {
        theme       => 'default',
        sort        => 1,
        license     => '',
        markdown    => 0,
        description => '',
    }
});
my $theme      = $config->{theme};
$config->{public_url} .= '/' unless $config->{public_url} =~ m#/$#;

# Copy theme
rmtree("$Bin/public");
dircopy("$Bin/themes/$theme", "$Bin/public");
fcopy("$Bin/themes/default/tags.html",    "$Bin/public/tags.html")    unless (-e "$Bin/public/tags.html");
fcopy("$Bin/themes/default/js/tags.js",   "$Bin/public/js/tags.js")   unless (-e "$Bin/public/js/tags.js");
fcopy("$Bin/themes/default/search.html",  "$Bin/public/search.html")  unless (-e "$Bin/public/search.html");
fcopy("$Bin/themes/default/js/search.js", "$Bin/public/js/search.js") unless (-e "$Bin/public/js/search.js");
fcopy("$Bin/themes/default/js/alpine.min.js",     "$Bin/public/js/alpine.min.js")     unless (-e "$Bin/public/js/alpine.min.js");
fcopy("$Bin/themes/default/js/alpine.min.js.map", "$Bin/public/js/alpine.min.js.map") unless (-e "$Bin/public/js/alpine.min.js.map");

# Start to modify the index file
my $url     = Mojo::URL->new($config->{public_url});
my $file    = Mojo::File->new('public/index.html');
my $license = $config->{license};
my $index   = open_index();

## Atom feed
   $url  = Mojo::URL->new($config->{public_url});
my $iso  = DateTime::Format::RFC3339->format_datetime(DateTime->now());
my $feed = XML::Atom::SimpleFeed->new(
    title   => $config->{title},
    id      => $url->to_string,
    updated => $iso,
    author  => $config->{author},
    link    => {
        rel  => 'self',
        href => $url->path($url->path->merge('feed.atom'))->to_string
    },
    '-encoding' => 'utf-8'
);

## Epub
my $cover = Mojo::DOM->new(decode('UTF-8', Mojo::File->new('public/epub/OPS/cover.xhtml')->slurp));
$cover->find('title')
      ->first
      ->content($config->{title});
$cover->find('#header-title')
      ->first
      ->content($config->{title});
$cover->find('#author')
      ->first
      ->content($config->{author});
$cover->find('#license')
      ->first
      ->content($license);
Mojo::File->new('public/epub/OPS/cover.xhtml')->spurt(encode('UTF-8', $cover->to_string));

my $nav = Mojo::DOM->new(decode('UTF-8', Mojo::File->new('public/epub/OPS/nav.xhtml')->slurp));
$nav->find('html')
    ->first
    ->attr(lang       => $config->{language})
    ->attr('xml:lang' => $config->{language});
my $ol = $nav->find('ol')->first;

my $toc = Mojo::DOM->new(decode('UTF-8', Mojo::File->new('public/epub/OPS/toc.ncx')->slurp));
$toc->find('text')
    ->first
    ->content($config->{title});
my $navmap = $toc->find('navMap')->first;

my $opf = Mojo::DOM->new(decode('UTF-8', Mojo::File->new('public/epub/OPS/content.opf')->slurp));
$opf->find('#uuid_id')
    ->first
    ->content($url->to_string);
$opf->find('#dclanguage')
    ->first
    ->content($config->{language});
$opf->find('#dctitle')
    ->first
    ->content($config->{title});
$opf->find('#dccreator')
    ->first
    ->content($config->{author});
$opf->find('#dcdate')
    ->first
    ->content($iso);
$opf->find('meta[property="dcterms:modified"]')
    ->first
    ->content($iso);
my $manifest = $opf->find('manifest')->first;
my $spine    = $opf->find('spine')->first;

my $efile = Mojo::DOM->new(decode('UTF-8', Mojo::File->new('public/epub/content.xhtml')->slurp));
$efile->find('html')
      ->first
      ->attr(lang       => $config->{language})
      ->attr('xml:lang' => $config->{language});

## (re)Initialize variables
$url = Mojo::URL->new($config->{public_url});
my $tags   = {};
my $search = {};
my $https  = ($url->scheme eq 'https');
my (@entries, @pages);
my $regex;
   $regex = join('|', @{$config->{hidden_tags}}) if $config->{hidden_tags};
### Pagination
my $pagination = $config->{pagination} || 0;
   $pagination = 0 unless $pagination > 0;
   $pagination = floor($pagination);
my $page       = 1;
### Where to insert the toots
my $c = $index->find('#content')->first;
$c->append_content("                    <p><a href=\"".slugify($config->{title}).".epub\">Epub</a> <a href=\"tags.html\">#Étiquettes</a> <a href=\"search.html\">Recherche</a></p>\n");
### Date formatting stuff
my $f = DateTime::Format::RFC3339->new();
my $l = DateTime::Locale->load($config->{language});
### Sort URLs
if (!defined($config->{urls})) {
    # You can use an other file for your urls
    my $config_urls = $m->plugin(Config => {
        file    => File::Spec->catfile($Bin, 'urls.conf'),
    });
    $config->{urls} = $config_urls->{urls} if defined($config_urls->{urls});
}
my $urls = c(@{$config->{urls}});
   $urls = $urls->sort(
    sub {
        my $a2 = $a;
        $a2 =~ s#^.*/([^/]+)$#$1#;
        my $b2 = $b;
        $b2 =~ s#^.*/([^/]+)$#$1#;
        $a2 <=> $b2
    }
) if $config->{sort};
   $urls  = $urls->reverse if $config->{reverse};
## Create needed dirs
mkdir 'public/epub/OPS/text';
mkdir 'cache' unless -e 'cache';

## URLs processing
# fetching toots, attachments, formatting, creating html, atom feed, epub…
my $n    = 0;
my %versions = ();
$urls->each(
    sub {
        my ($e, $nb) = @_;
        my ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2, $poll) = process_entry($e, $nb);
        format_and_insert_toot($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2, $poll);
    }
);

$c->append_content("               ");

# Write the index.html file
$file->spurt(encode('UTF-8', $index));

# Update the tags.html <head> tag
my $head = Mojo::DOM->new($index)
                    ->find('head')
                    ->first
                    ->append_content("    <script src=\"js/tags.js\"></script>\n")
                    ->append_content("        <script src=\"js/alpine.min.js\" defer></script>\n    ");
$head->find('link[rel="alternate"]')->first->remove;
my $tags_file = Mojo::File->new('public/tags.html');
my $tags_html = Mojo::DOM->new(decode('UTF-8', $tags_file->slurp));
$tags_html->find('head')->first->replace($head->to_string);
$tags_file->spurt(encode('UTF-8', $tags_html));

# Write the tags.json file
Mojo::File->new('public/js/tags.json')->spurt(encode_json($tags));

# Update the search.html <head> tag
$head->find('script[src="js/tags.js"]')->first->remove;
$head->find('script[src="js/alpine.min.js"]')->first->remove;
$head->append_content("    <script src=\"js/elasticlunr.min.js\"></script>\n")
     ->append_content("        <script src=\"js/search.js\"></script>\n")
     ->append_content("        <script src=\"js/alpine.min.js\"></script>\n");
my $search_file = Mojo::File->new('public/search.html');
my $search_html = Mojo::DOM->new(decode('UTF-8', $search_file->slurp));
$search_html->find('head')->first->replace($head->to_string);
$search_file->spurt(encode('UTF-8', $search_html));

# Write the search.json file
Mojo::File->new('public/js/search.json')->spurt(encode_json($search));

# Add entries in the atom feed
my $en = c(@entries);
   $en = $en->reverse unless $config->{reverse};
my $max = ($en->size < 20) ? $en->size -1 : 19;
   $en = c(@{$en->to_array}[0 .. $max]);
$en->each(
    sub {
        my ($e, $num) = @_;
        my $content = Mojo::DOM->new($e->{content});
        $content->find('article a h2')->first->parent->remove;
        $feed->add_entry(
            title   => $e->{title},
            id      => Mojo::URL->new($config->{public_url})->to_string.'#'.$e->{id},
            link    => Mojo::URL->new($config->{public_url})->to_string.$e->{id}.'.html',
            updated => $e->{updated},
            content => $content->to_string,
        );
    }
);
## Write the atom feed
my $feed_string = $feed->as_string;
$feed_string =~ s#<\?xml version="1\.0" encoding="utf-8"\?>#<?xml version="1.0" encoding="utf-8"?>\n<?xml-stylesheet type="text/xsl" href="css/atom-to-html.xsl"?>#;
Mojo::File->new('public/feed.xml')->spurt($feed_string);
# Keep the old name working
unlink('public/feed.atom') if -e 'public/feed.atom';
symlink('feed.xml', 'public/feed.atom');

# Write the epub files
my $pa = c(@pages);
   $pa = $pa->reverse if $config->{reverse};
$pa->each(
    sub {
        my ($e, $num) = @_;
        my $date    = $e->{date};
        my $time    = $e->{time};
        my $content = $e->{content};
        my $tmp     = $efile;

        $tmp->find('.column')
              ->first
              ->content($content);
        $tmp->find('title')
              ->first
              ->content("$date $time");
        Mojo::File->new("public/epub/OPS/text/$num.xhtml")->spurt(encode('UTF-8', $tmp->to_string));

        $manifest->append_content("    <item id=\"t$num\" href=\"text/$num.xhtml\" media-type=\"application/xhtml+xml\" />\n");
        $spine->append_content("    <itemref idref=\"t$num\" />\n");
        $navmap->append_content("    <navPoint id=\"NavPoint-$num\" playOrder=\"$num\"><navLabel><text>$date $time</text></navLabel><content src=\"text/$num.xhtml\"/></navPoint>\n");
        $ol->append_content("\n                <li><a href=\"text/$num.xhtml\">$date $time</a></li>");
    }
);
my $num = $pa->size + 1;
$manifest->append_content("  ");
   $spine->append_content("    <itemref idref=\"nav\" />\n  ");
  $navmap->append_content("    <navPoint id=\"NavPoint-$num\" playOrder=\"$num\"><navLabel><text>Table des matières</text></navLabel><content src=\"nav.xhtml\"/></navPoint>\n");
      $ol->append_content("\n                <li><a href=\"nav.xhtml\">Table des matières</a></li>\n            ");
Mojo::File->new('public/epub/OPS/toc.ncx')->spurt(encode('UTF-8', $toc->to_string));
Mojo::File->new('public/epub/OPS/nav.xhtml')->spurt(encode('UTF-8', $nav->to_string));
Mojo::File->new('public/epub/OPS/content.opf')->spurt(encode('UTF-8', $opf->to_string));

my $zip = Archive::Zip->new();
$zip->addFile('public/epub/mimetype', 'mimetype');
$zip->addTree('public/epub/OPS/', 'OPS');
$zip->addTree('public/epub/META-INF', 'META-INF');

unless ($zip->writeToFileNamed('public/'.slugify($config->{title}).'.epub') == AZ_OK) {
    die 'write error';
}
rmtree('public/epub') unless $ENV{LAST_DEBUG};

sub open_index {
    my $page        = shift // '/index.html';
    my $description = shift;

    my $file   = Mojo::File->new("$Bin/themes/$theme/index.html");
    my $index  = Mojo::DOM->new(decode('UTF-8', $file->slurp));
    my $back_to_homepage = $index->find('#back-to-homepage')->first;
    $back_to_homepage->remove if ($back_to_homepage || $page eq '/index.html');
    $index->find('html')
          ->first
          ->attr(lang => $config->{language});
    $index->find('title')
          ->first
          ->content($config->{title});
    my $meta_desc   = $index->find('meta[name="description"]') ->first;
       $meta_desc->attr(content => $config->{description})     if $meta_desc;
    my $meta_author = $index->find('meta[name="author"]')      ->first;
       $meta_author->attr(content => $config->{author})        if $meta_author;
    my $meta_title  = $index->find('meta[property="og:title"]')->first;
       $meta_title->attr(content => $config->{title})          if $meta_title;
    my $public_url  = $config->{public_url};
    $public_url =~ s#/$##;
    my $meta_url    = $index->find('meta[property="og:url"]')  ->first;
       $meta_url->attr(content => $public_url.$page)           if $meta_url;
    my $meta_image  = $index->find('meta[property="og:image"]')->first;
       $meta_image->attr(content => $public_url.'/img/favicon.png') if $meta_image;
    if ($https) {
        $index->find('head')
              ->first
              ->at('meta[property="og:image"]')
              ->append("\n        ".sprintf('<meta property="og:image:secure_url" content="%s/img/favicon.png" />', $public_url));
    }
    if (defined($config->{meta})) {
        my $insert_here = $index->find('head')
              ->first
              ->at('meta[property="og:description"]');
        while (my ($attr, $value) = each %{$config->{meta}}) {
            while (my ($name, $content) = each %{$value}) {
                $insert_here->append("\n        ".sprintf('<meta %s="%s" content="%s" />', $attr, $name, $content));
            }
        }
    }

    $description       = $config->{description} unless $description;
    my $meta_og_desc   = $index->find('meta[property="og:description"]')->first;
       $meta_og_desc->attr(content => $description)                     if $meta_og_desc;
    my $meta_og_locale = $index->find('meta[property="og:locale"]')     ->first;
       $meta_og_locale->attr(content => $config->{language})            if $meta_og_locale;
    my $header_title   = $index->find('#header-title')                  ->first;
       $header_title->content($config->{title})                         if $header_title;
    my $id_author      = $index->find('#author')                        ->first;
       $id_author->content($config->{author})                           if $id_author;
    my $rel_canonical  = $index->find('link[rel="canonical"]')          ->first;
       $rel_canonical->attr(href => $public_url.$page)                  if $rel_canonical;
    my $rel_alternate  = $index->find('link[rel="alternate"]')          ->first;
       $rel_alternate->attr(href => $url->path($url->path->merge('feed.xml'))->to_string) if $rel_alternate;

    my $id_license = $index->find('#license')->first;
    if ($license) {
        $license = "<img alt=\"$license\" src=\"img/$license.png\">" if ($license eq 'cc-0');
        $license = "<img alt=\"$license\" src=\"img/$license.png\">" if ($license eq 'public-domain');
        $license = "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>" if ($license eq 'cc-by-nc-nd');
        $license = "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>" if ($license eq 'cc-by-nc-sa');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by-nd/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"   if ($license eq 'cc-by-nd');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"      if ($license eq 'cc-by');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"   if ($license eq 'cc-by-sa');

        $id_license->content($license) if $id_license;
    } else {
        $id_license->remove            if $id_license;
    }

    return $index;
}

sub process_entry {
    my ($i, $num, $prev_id) = @_;

    if (reftype(\$i) eq 'SCALAR') {
        ## Get the ID of the toot
        my $id = $i;
           $id =~ s#^.*/([^/]+)$#$1#;

        ## Get only the pod's hostname and scheme
        my $host = Mojo::URL->new($i)->path('');

        my $cache = (-e "cache/$id.json");

        my $msg   = "Processing $i";
           $msg  .= " from cache" if $cache;
        say $msg;

        my $res;
           $res = $ua->get($host->path("/api/v1/statuses/$id"))->result unless $cache;
        if ($cache || $res->is_success) {
            Mojo::File->new("cache/$id.json")->spurt(encode_json($res->json)) unless $cache;
            ## Get only the targeted toot, not the replies
            my $json    = ($cache) ? decode_json(Mojo::File->new("cache/$id.json")->slurp) : $res->json;

            my $search_id = (defined($prev_id)) ? $prev_id : $id;

            # Create tags index
            c(@{$json->{tags}})->each(sub {
                my ($t, $num) = @_;
                if (!($regex) || $t !~ m#$regex#) {
                    my $tag         = $t->{name};

                    push @{$tags->{$tag}}, $search_id if     defined $tags->{$tag};
                    $tags->{$tag} = [$search_id]      unless defined $tags->{$tag};
                }
            });

            my ($content, $dt, $attach, $jpoll) = extract_from_json($json);

            $content         = filter_content($content);
            my $content_html = filter_tags($content);

            my ($date, $time) = format_time($dt);

            my ($attachments, $attachments2) = fetch_attachments($attach, $num);

            ## Mix date, time, content and attachments
            # Modifs persos
            if ($lc_loaded) {
                $content = LastCustom::custom_filter($content, $id);
            }

            if ($config->{markdown} && $content =~ m#([^%])?%md#) {
                $content = process_md($content);
            }

            my $poll = format_poll($jpoll);

            return ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2, $poll);
        } elsif ($res->is_error) {
            die "Error while fetching $i: $res->message";
        }
    } else {
        my ($content, $attachments, $attachments2, $poll, $date, $time, $id, $dt) = ('', '', '', '');
        c(@{$i})->each(
            sub {
                my ($e, $nb) = @_;

                my ($tmp_content, $tmp_i, $tmp_date, $tmp_time, $tmp_attachments, $tmp_id, $tmp_dt, $tmp_num, $tmp_attachments2, $tmp_poll) = process_entry($e, $nb, $id);
                if ($nb == 1) {
                    $i            = $tmp_i;
                    $date         = $tmp_date;
                    $time         = $tmp_time;
                    $id           = $tmp_id;
                    $dt           = $tmp_dt;
                    $content      = $tmp_content;
                    $attachments  = $tmp_attachments;
                    $attachments2 = $tmp_attachments2;
                    $poll         = $tmp_poll;
                } else {
                    if ($tmp_content) {
                        my $tmp_dom   = $tmp_content;
                        my $dom       = $content;
                        $tmp_dom      =~ s#(^<p>|</p>$)##g;
                        $dom          =~ s#(^<p>|</p>$)##g;
                        $content      = Mojo::DOM->new("<p>$dom$tmp_dom</p>");
                    }
                    if ($attachments) {
                        my $tmp_dom   = Mojo::DOM->new($tmp_attachments)->at('p')->content;
                        my $dom       = Mojo::DOM->new($attachments)->at('p')->content;
                        $attachments  = Mojo::DOM->new($attachments)->at('p')->replace("<p>$dom$tmp_dom</p>");
                    }
                    if ($attachments2) {
                        my $tmp_dom   = Mojo::DOM->new($tmp_attachments2)->at('p')->content;
                        my $dom       = Mojo::DOM->new($attachments2)->at('p')->content;
                        $attachments2 = Mojo::DOM->new($attachments2)->at('p')->replace("<p>$dom$tmp_dom</p>");
                    }
                }
            }
        );
        return ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2, $poll);
    }
}

sub extract_from_json {
    my $json = shift;

    ## Get the content of the toot
    my $content = Mojo::DOM->new($json->{content});
    ## Get the metadata (ie date and time) of the toot
    my $dt      = $f->parse_datetime($json->{created_at})->set_locale($config->{language});
    $dt->set_time_zone($config->{timezone}) if defined $config->{timezone};
    ## Get the attachments of the toot
    my $attach = c(@{$json->{media_attachments}});
    ## Get poll
    my $poll   = $json->{poll} // '';

    return ($content, $dt, $attach, $poll);
}

sub filter_content {
    my $content = shift;

    ## Remove style attribute
    ## Replace emoji with unicode characters
    $content->find('img.emojione')->each(
        sub {
            my ($e, $num) = @_;
            $e->replace($e->attr('alt'));
        }
    );
    ## Remove the configured hashtags
    $content->find('a.mention.hashtag')->each(
        sub {
            my ($e, $num) = @_;
            my $href = $e->attr('href');
            $e->remove if $href =~ m#tags/($regex)$#i;
        }
    ) if $regex;

    return $content;
}

sub filter_tags {
    my $content = Mojo::DOM->new(shift);

    $content->find('a.mention.hashtag')->each(
        sub {
            my ($e, $num) = @_;
            my $tag = Mojo::URL->new($e->attr('href'))->path->parts->[-1];
            $e->attr('href' => sprintf('tags.html#%s', lc($tag)));
        }
    );

    return $content;
}

sub format_poll {
    my $jpoll = shift;
    my $poll  = '';

    if ($jpoll) {
        my $total = $jpoll->{votes_count};
        my $ul    = Mojo::DOM->new('<ul class="poll"></ul>');

        for my $option (@{$jpoll->{options}}) {
            my $li      = Mojo::DOM->new('<li></li>');
            my $percent = 100 * $option->{votes_count} / $total;
            my $label   = sprintf('<label class="poll_option"><span class="poll_number">%d%%</span><span class="poll_text">%s</span></label>', nearest(0.1, $percent), $option->{title});
            my $bar     = sprintf('<span class="poll_chart" style="width:%f%%">', $percent);
            $li->at('li')
               ->append_content($label)
               ->append_content($bar);

            $ul->at('ul')->append_content($li);
        }
        $ul->at('ul')->append(sprintf('<span class="poll_voters">Votes: %s</span>', $total));
        $poll = $ul->to_string;
    }

    return $poll;
}

sub format_time {
    my $dt = shift;

    ## Format date and time
    my $date    = $dt->format_cldr($l->date_format_full);
    my $time    = $dt->format_cldr($l->time_format_medium);

    return ($date, $time),
}

sub fetch_attachments {
    my ($attach, $num) = @_;

    ## Store the attached images locally
    my @imgs;
    my @imgs2;
    $attach->each(
        sub {
            my ($e, $inum) = @_;

            my $src = $e->{url};
               $src =~ s#\?(.*)##;
            my $q   = $1;
               $src =~ m#/([^/]*)$#;
            my $n   = $1;
               $src = Mojo::URL->new($src);

            ## Attachments cache
            my $acache = (-e "cache/img/$n" && -e "cache/img/$n.meta");
            my $msg = sprintf("  Fetching image: %s", $src);
               $msg .= " from cache" if $acache;
            say $msg;

            my $img = "img/$n";;
            my $rmime;
            if ($acache) {
                # Get file metadata from cache
                $rmime = decode_json(Mojo::File->new("cache/$img.meta")->slurp);
                # Copy file
                Mojo::File->new("cache/$img")->copy_to("public/$img")->copy_to("public/epub/OPS/$img");
            } else {
                my $r = $ua->get($src)->result;
                if ($r->is_success) {
                    my $body = $r->body;
                      $rmime = $r->headers->content_type;

                    # Create cache
                    mkdir 'cache/img' unless -d 'cache/img';
                    Mojo::File->new("cache/$img")->spurt($body);
                    Mojo::File->new("cache/$img.meta")->spurt(encode_json($rmime));

                    # Copy file
                    Mojo::File->new("public/$img")->spurt($body);
                    Mojo::File->new("public/epub/OPS/$img")->spurt($body);
                } elsif ($r->is_error) {
                    die sprintf("Error while fetching %s: %s", $src, $m->dumper($r->message));
                }
            }
            if ($e->{type} eq 'image') {
                push @imgs, "<img class=\"u-max-full-width\" src=\"$img\" alt=\"\">";
                push @imgs2, "<img class=\"u-max-full-width\" src=\"../$img\" alt=\"\" />";
            } elsif ($e->{type} eq 'video') {
                push @imgs, "<video class=\"u-max-full-width\" src=\"$img\" alt=\"\"></video>";
                push @imgs2, "<video class=\"u-max-full-width\" src=\"../$img\" alt=\"\" ></video>";
            }
            $manifest->append_content("    <item id=\"i$num-$inum\" href=\"$img\" media-type=\"$rmime\" />\n");
        }
    );
    my $attachments  = (scalar @imgs)  ? "<div><p>@imgs</p></div>"  : '';
    my $attachments2 = (scalar @imgs2) ? "<div><p>@imgs2</p></div>" : '';

    return ($attachments, $attachments2);
}

sub process_md {
    my $content = shift;

    $content =~ s#%%md#%%LAST_MARKDOWN%%#g;
    $content =~ s#%md##g;
    $content =~ s#%%LAST_MARKDOWN%%#%md#g;
    # Images
    $content =~ s#!\[(.*?)\]\(<a href="([^"]*)".*?</a>\)#![$1]($2)#gm;
    # Links with text
    $content =~ s#\[(.*?)\]\(<a href="([^"]*)".*?</a>\)#[$1]($2)#gm;
    # Links
    $content =~ s#&lt;<a href="([^"]*)".*?</a>&gt;#<$1>#gm;
    my $md;
    my $html = encode('UTF-8', $content);
    pandoc -f => 'markdown', -t => 'html', { in => \$html, out => \$md };
    say $md;
    $content = decode('UTF-8', $md);

    return $content;
}

sub format_and_insert_toot {
    my ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2, $poll) = @_;
    $content =~ s#<p></p>|</br>##g;
    $content =~ s#<br>#<br />#g;
    my $content_html = filter_tags($content);
    my $all_text     = $content_html->all_text();
    $content_html    = $content_html->to_string;

    my $append = <<EOF;

                    <article>
                        <a href="$id.html">
                            <h2>$date <span style="font-size: 0.5em;">$time</span></h2>
                        </a>
                        $content_html
                        $attachments
                        $poll
                        <div>
                            <a href="$i">
                                <em>Source</em>
                            </a>
                        </div>
                    </article>
                    <hr>

EOF

    ## Insert the toot
    $c->append_content($append);

    ## Fill the search index
    my $alt = '';
    Mojo::DOM->new($attachments)
             ->find('img')
             ->each(sub {
                 my ($e, $num) = @_;
                 $alt .= '. '.$e->attr('alt') if $e->attr('alt');
    });
    my $poll_text = '';
    Mojo::DOM->new($poll)
             ->find('.poll_text')
             ->each(sub {
                 my ($e, $num) = @_;
                 $poll_text .= '. '.$e->all_text;
    });
    $search->{$id} = {
        id          => $id,
        text        => $all_text,
        html        => $append,
        attachments => ($attachments) ? true : false,
        alt         => $alt,
        poll        => $poll_text
    };

    ## Toot page
    my $description = Mojo::DOM->new($content_html)->all_text;
    my $tpindex = open_index('/'.$id.'.html', $description);
    my $tpc     = $tpindex->find('#content')->first;
    $tpc->append_content($append);
    Mojo::File->new("public/$id.html")->spurt(encode('UTF-8', $tpindex));

    ## Paginate
    if ($pagination && (($num % $pagination) == 0 || ($num == $urls->size))) {
        my $prec = ($page == 2) ? 'index' : 'page'.($page - 1);

        if ($num == $urls->size) {
            $c->append_content("\n                    <p class=\"u-pull-left\"><a class=\"button button-primary\" href=\"$prec.html\">⇐ Page précédente</a></p>");
        } else {
            $page++;
            if ($page == 2) {
                $c->append_content("\n                    <p class=\"u-pull-right\"><a class=\"button button-primary\" href=\"page$page.html\">Page suivante ⇒</a></p>");
            } else {
                $c->append_content("\n                    <p class=\"u-pull-left\"><a class=\"button button-primary\" href=\"$prec.html\">⇐ Page précédente</a></p><p class=\"u-pull-right\"><a class=\"button button-primary\" href=\"page$page.html\">Page suivante ⇒</a></p>");
            }
            $c->append_content("               ");

            $file->spurt(encode('UTF-8', $index));

            $file  = Mojo::File->new("public/page$page.html");
            $index = open_index('/page'.$page.'.html');
            $c     = $index->find('#content')->first;
            $c->append_content("                    <p><a href=\"".slugify($config->{title}).".epub\">Epub</a> <a href=\"tags.html\">#Étiquettes</a> <a href=\"search.html\">Recherche</a></p>\n");
        }
    }

    ## Create entries for the atom feed
    push @entries, {
        title   => "$date $time",
        id      => "$id",
        updated => DateTime::Format::RFC3339->format_datetime($dt),
        content => $append
    };

    ## Epub
    push @pages, {
        num     => $num,
        date    => $date,
        time    => $time,
        content => "<h1>$date <span style=\"font-size: 0.5em;\">$time</span></h1><hr/>$content$attachments2$poll<div><a href=\"$i\"><em>Source</em></a></div>"
    };
}
